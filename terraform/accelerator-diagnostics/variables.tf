variable "aks_cluster_id" {}
variable "aks_resource_group_name" {}
variable "location" {}
variable "aks_log_analytics_workspace_id" {}
variable "project_name" {}
